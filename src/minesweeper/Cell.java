package minesweeper;

class Cell {
    
    private final int row;
    private final int column;
    
    private int type;        
    private boolean isCovered = true;
    private boolean isMine    = false;
    
    private final int EMPTY_CELL = 0;
    private final int FLAG_CELL  = 11;
    private final int QUESTION_CELL = 12;

    public Cell(int row, int column) {
        this.row        = row;
        this.column     = column;
        this.type       = 0;
        this.isCovered  = true;
        this.isMine     = false;
    }

    public int getRow() {
        return this.row;
    }
    
    public int getColumn() {
        return this.column;
    }
    
    public int getType() {
        return this.type;
    }   
    
    public void setType(int type) {
        this.type = type;
    }

    public void setTypeEmpty() {
        this.type = EMPTY_CELL;
    }
    
    public void addMine() {
        this.isMine = true;
        this.type   = 9;
    }
    
    public void Cover() {
        this.isCovered = true;
    }
    
    public void unCover() {
        this.isCovered = false;
    }    
    
    public boolean isCovered() {
        return this.isCovered;
    }
    
    public boolean isMineCell() {
        return this.isMine;
    }
    
    public boolean isEmptyCell() {
        return this.getType() == EMPTY_CELL;
    }

    public boolean isFlaggedCell() {
        return this.getType() == FLAG_CELL;
    }

    public boolean isQuestionnableCell() {
        return this.getType() == QUESTION_CELL;
    }

    public void setTypeNext() {
        System.out.println("setting type "+(this.getType()+1));
        if (this.getType() == 12) {
            this.setType(10);
        }
        else
            this.setType(this.getType()+1);
    }
}
