package minesweeper;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.util.Random;
import java.util.*;

import javax.swing.*;


class Board extends JPanel {
    
    private Image[] tiles;
    private Cell[]  minefield; 

    private final int COLUMNS       = 9;
    private final int ROWS          = 9;
    private final int TOTAL_CELLS   = COLUMNS * ROWS;
    private final int TOTAL_MINES   = 4;
    
    private final int CELL_SIZE     = 15;
    
    private final int MINE_CELL     = 9;
    
    private boolean   newGame       = false;
    
    
    
    private BoardControl boardControl;
    
    private boolean isNewGame() {
        return this.newGame;
    }
    
    private void startNewGame() {
        this.newGame = true;
        System.out.println("isNewGame");
    } 
    
    private void finishGame() {
        this.newGame = false;
    }
    
    public Board() {
        boardControl = new BoardControl();
        add(boardControl);
        
        setBorder(BorderFactory.createLineBorder(Color.red, 2));
        setPreferredSize(new Dimension(300,300));
        addMouseListener(new MinesAdapter());
        startMinesweeper();    
    }
    
    void startMinesweeper() {
        
        addImagesToGame();
        //paintInSeparateBuffer();
        
        startNewGame();
        
        //JButton startButton = new JButton("Wow");
        //add(startButton, BorderLayout.NORTH);
                
        addMinefieldCells();
        addRandomMineCells();
        findCellsSurroundingMines();
    }
    
    void addImagesToGame() {
        tiles = new Image[13];
        for (int index=0; index<13;index++) {
            tiles[index] = (createImageIcon("img/"+index+".gif")).getImage();      
        }
        //System.out.println("added images to game");
    }
    
    ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = getClass().getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            //System.err.println("Couldn't find file: " + path);
            return null;
        }
    }
    //protected void paintInSeparateBuffer() {
    //    setDoubleBuffered(true);
    //    //System.out.println("painted in separate buffer");
    //}

    
    private void addMinefieldCells() {
        minefield = new Cell[TOTAL_CELLS];
        for(int row = 1; row <= ROWS; row++) {
            for (int column = 1; column <= COLUMNS; column++) {
                minefield[getCellPosition(row,column)] = new Cell(row, column);
            }
        }
        //System.out.println("Added minefield cells");
    }   
       
    void addRandomMineCells() {
        Random random_mines_generator;
        random_mines_generator = new Random();
        
        for (int index = 0; index < TOTAL_MINES; index++) {
            int random_mine = random_mines_generator.nextInt(TOTAL_CELLS);
            minefield[random_mine].addMine();
        }
        //System.out.println("Added random mine cells");
    }
    
    void findCellsSurroundingMines() {
        for (int index = 0; index < TOTAL_CELLS; index++) {
            if (minefield[index].isMineCell()) {
                incrementCellsSurroundingMines(minefield[index].getRow(),minefield[index].getColumn());
            }
        }
        //System.out.println("Found cells surrounding mines");
    }
    
    void incrementCellsSurroundingMines(int mine_row, int mine_column) {
        for (int row = mine_row-1; row <= mine_row+1; row++) {
            for (int column = mine_column-1; column <= mine_column+1; column++) {
                if (isCell(row,column)) {
                    //int type = minefield[getCellPosition(row,column)].getType();
                    //if (type != MINE_CELL) {
                    //    minefield[getCellPosition(row,column)].setType(type+1);
                    //}
                    Cell cell = minefield[getCellPosition(row,column)];
                    if (!cell.isMineCell()) {
                        cell.setTypeNext();
                    }
                }
            }
        }
    }
    
    ArrayList getSurroundingCells(Cell cell) {
        int cell_row = cell.getRow();
        int cell_column = cell.getColumn();
        //System.out.println("getSurroundingCells("+cell_row+","+cell_column+")");
        ArrayList<Cell> surroundingCells = new ArrayList<Cell>();
        for (int row = cell_row-1; row <= cell_row+1; row++) {
            for (int column = cell_column-1; column <= cell_column+1; column++) {
                if (isCell(row,column)) {
                    if (row == cell_row && column == cell_column) {
                    }
                    else {
                        int position = getCellPosition(row,column);
                        Cell currentCell = minefield[position];
                        surroundingCells.add(currentCell);
                        //System.out.println("("+currentCell.getRow()+","+currentCell.getColumn()+")");
                    }
                }
            }
        }
        return surroundingCells;
    }
    
   void iterateOverSurroundingCells(ArrayList surroundingCells) {
       
       if(surroundingCells.isEmpty()) {
           return;
       }
       
       Iterator iterator = surroundingCells.iterator();
       
       ArrayList<Cell> newSurroundingCells;
       newSurroundingCells = new ArrayList();
       
       while(iterator.hasNext()) {
           Cell currentCell = (Cell)iterator.next();
           //System.out.println(" ("+currentCell.getRow()+","+currentCell.getColumn()+")");
           
           currentCell.unCover();
           
           if (currentCell.isEmptyCell()) {
               
               ArrayList<Cell> newShit;
               newShit = getSurroundingCells(currentCell);

               for (Cell shit : newShit) {
                   if (shit.isCovered()) {
                       newSurroundingCells.add(shit);
                   }
               }
               
           }
       }     
       HashSet<Cell> hs = new HashSet();
       hs.addAll(newSurroundingCells);
       newSurroundingCells.clear();
       newSurroundingCells.addAll(hs);
       
       iterateOverSurroundingCells(newSurroundingCells);
   }     
    
    boolean checkCoveredCellsForMines() {
        int mines_left = 0;
        for (int i=0; i < TOTAL_CELLS; i++) {
            Cell cell = minefield[i];
            if (cell.isCovered()) {
                if (cell.getType() == MINE_CELL || cell.isFlaggedCell() || cell.isQuestionnableCell()) {
                    mines_left++;
                }
                else {
                    return false;
                }
            }
        }
        if (mines_left == TOTAL_MINES) {
            return true;
        }
        return false;
    }
       
       
       
    boolean isCell(int row, int column) {
        if (row > 0 && row <= ROWS && column > 0 && column <= COLUMNS) {
            return true;
        }
        else {
            return false;
        }
    }
    
    int getCellPosition(int row, int column) {
        //System.out.println("Cell position is: "+(((row-1)*COLUMNS+(column-1))));
        return (row-1)*COLUMNS+(column-1);
    }
        
    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        for (int index=0; index < TOTAL_CELLS; index++) {
            drawProperTiles(graphics,index);
        }
    }
    
    private void drawProperTiles(Graphics graphics, int index) {
        Cell cell = minefield[index];
        int type = 10;
        
        if (!cell.isCovered())
            type = cell.getType();
        else if (cell.isFlaggedCell() || cell.isQuestionnableCell())
            type = cell.getType();
        
        graphics.drawImage(tiles[type],cell.getRow()*CELL_SIZE,cell.getColumn()*CELL_SIZE,this);
    }
    
   
    private class MinesAdapter extends MouseAdapter {
        
        public void mousePressed(MouseEvent e) {
            
                int x = e.getX();
                int y = e.getY();

                int row     = x/CELL_SIZE; 
                int column  = y/CELL_SIZE;

            if (row > 0 && row <= ROWS && column > 0 && column <= COLUMNS && isNewGame()) {

                Cell clickedCell = minefield[getCellPosition(row,column)];

                // Left Mouse Click
                if (SwingUtilities.isLeftMouseButton(e)) {
                    clickedCell.unCover();

                    if (clickedCell.isEmptyCell()) {
                        ArrayList<Cell> surroundingCells;
                        surroundingCells = getSurroundingCells(clickedCell);
                        iterateOverSurroundingCells(surroundingCells);
                    }
                    else if (clickedCell.isMineCell()) {
                        System.err.println("YOU LOSE!");
                        finishGame();
                        startMinesweeper();
                    }

                    if (checkCoveredCellsForMines()) {
                        System.out.println("YOU WON!");
                        finishGame();
                        startMinesweeper();
                    }
                }

                // Right Mouse Click (=> flag => question => empty)
                else if (SwingUtilities.isRightMouseButton(e)) {
                    System.out.print("right mouse click [");

                    if (clickedCell.isCovered()) {

                        System.out.print("covered,");
                        if (!clickedCell.isFlaggedCell() && !clickedCell.isQuestionnableCell()) {
                            System.out.print("not flagged, not questionable,");
                            clickedCell.setTypeNext();
                        }
                        else if (clickedCell.isFlaggedCell()) {
                            System.out.print("flagged,");
                            clickedCell.setTypeNext();
                        }
                        else if (clickedCell.isQuestionnableCell()) {
                            System.out.print("questionable,");
                            if (clickedCell.isMineCell()) {
                                System.out.print("making a mine,");
                                clickedCell.addMine();
                            }
                            else {
                                System.out.print("making empty,");
                                clickedCell.setTypeEmpty();
                            }
                        }

                    }
                    System.out.println("]");
                }
                repaint();
            }
        }
    }
}