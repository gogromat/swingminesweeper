package minesweeper;

import javax.swing.*;
import java.awt.*;

public class Minesweeper extends JFrame {

    private JLabel  statusbar;
    private JButton startButton;
    
    
    private Minesweeper() {

        JPanel container = new JPanel();
        container.setLayout(new BoxLayout(container, BoxLayout.X_AXIS));

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500,500);
        setLocationRelativeTo(null);
        setTitle("Minesweeper");
                      
        JPanel topPanel = new JPanel();
        //topPanel.add(newGame);
        
        topPanel.setBorder(BorderFactory.createLineBorder(Color.black));
        //getContentPane().add(startButton, BorderLayout.NORTH);
        getContentPane().add(topPanel, BorderLayout.NORTH);
        getContentPane().add(new Board(), BorderLayout.CENTER);
        
        setResizable(true);
        setVisible(true);
    }
    
    public static void main(String[] args) {
        new Minesweeper();
    }
}